import { Controller, Get } from '@nestjs/common';
interface HealthResponse {
  status: string;
  server: {
    status: string;
  };
}

@Controller('/health')
export class HealthController {
  @Get()
  index(): HealthResponse {
    return {
      status: 'ok',
      server: {
        status: 'up',
      },
    };
  }
}
