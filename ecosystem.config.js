module.exports = {
  apps: [
    {
      name: "app1",
      script: "./services/app1/dist/main.js",
      watch: true,
      node_args: ["--inspect", "--inspect-port=9231"]
    },
    {
      name: "app2",
      script: "./services/app2/dist/main.js",
      watch: true,
      node_args: ["--inspect", "--inspect-port=9232"]
    }
  ]
};
