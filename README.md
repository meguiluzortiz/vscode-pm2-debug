# vscode-pm2-debug
### Tech

This example uses a number of open source projects to work properly:

* [NodeJs] - evented I/O for the backend
* [NestJs] - a progressive Node.js framework for server-side applications
* [Pm2] - daemon process manager that will help you manage and keep your application
* [Lerna] - a tool for managing JavaScript projects with multiple packages.
* [VsCode] - duh

### Installation

The app is tested with [Node.js](https://nodejs.org/) v10+.

Install the dependencies and devDependencies and start the server.

```sh
$ cd vscode-pm2-debug
$ npm install -d
$ npm run all
```